package com.rahmanarifofficial.challenge4.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.rahmanarifofficial.challenge4.R
import com.rahmanarifofficial.challenge4.auth.LoginFragment
import com.rahmanarifofficial.challenge4.databinding.FragmentHomeBinding
import com.rahmanarifofficial.challenge4.util.PreferencesUtil

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private lateinit var viewModel: HomeViewModel
    private lateinit var adapter: NoteAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentHomeBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObject()
        setupUI()
        setupEventUI()
    }

    private fun setupObject() {
        val factory = HomeViewModelFactory.getInstance(requireContext())
        viewModel = ViewModelProvider(requireActivity(), factory)[HomeViewModel::class.java]
    }

    private fun setupUI() {
        viewModel.notes.observe(viewLifecycleOwner) {
            adapter = NoteAdapter(it, { position ->
                AddEditNoteDialogFragment.show(
                    activity,
                    false,
                    getString(R.string.edit_note),
                    it[position].titleNote,
                    it[position].note
                ) { isAdd, titleNote, contentNote ->
                    if (!isAdd) {
                        viewModel.editNote(it[position].noteId, titleNote, contentNote)
                    }
                }
            }) { position ->
                DeleteDialogFragment.show(
                    activity
                ) {
                    viewModel.deleteNote(it[position].noteId ?: 0)
                }
            }
            binding.recyclerViewNote.adapter = adapter
            binding.recyclerViewNote.layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun setupEventUI() {
        binding.logoutText.setOnClickListener {
            PreferencesUtil.getInstance(requireActivity().applicationContext).isLogin = false
            findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
        }
        binding.btnAddNote.setOnClickListener {
            AddEditNoteDialogFragment.show(
                activity,
                true,
                getString(R.string.add_note)
            ) { isAdd, titleNote, contentNote ->
                if (isAdd) {
                    viewModel.insertNote(titleNote, contentNote)
                }
            }
        }
    }
}