package com.rahmanarifofficial.challenge4.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rahmanarifofficial.challenge4.databinding.ItemNoteBinding

class NoteAdapter(
    private val itemList: List<Note>,
    private val actionEdit: (Int) -> Unit,
    private val actionDelete: (Int) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = ItemNoteBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return DefaultViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is DefaultViewHolder -> {
                holder.bindItem(itemList[position], position, actionEdit, actionDelete)
            }
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }
}

class DefaultViewHolder(val itemBinding: ItemNoteBinding) :
    RecyclerView.ViewHolder(itemBinding.root) {
    fun bindItem(
        item: Note,
        position: Int,
        actionEdit: (Int) -> Unit,
        actionDelete: (Int) -> Unit
    ) {
        itemBinding.titleNote.text = item.titleNote
        itemBinding.contentNote.text = item.note
        itemBinding.editNoteButton.setOnClickListener { actionEdit(position) }
        itemBinding.deleteNoteButton.setOnClickListener { actionDelete(position) }
    }
}