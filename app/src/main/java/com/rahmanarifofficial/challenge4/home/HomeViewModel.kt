package com.rahmanarifofficial.challenge4.home

import androidx.lifecycle.ViewModel
import com.rahmanarifofficial.challenge4.database.NoteDao

class HomeViewModel(
    val database: NoteDao
) : ViewModel() {

    val notes = database.getAllNote()

    fun insertNote(titleNote: String, noteContent: String? = null) {
        val note = Note(titleNote = titleNote, note = noteContent)
        database.insert(note)
    }

    fun editNote(noteId: Int?, titleNote: String, noteContent: String? = null) {
        val note = Note(noteId = noteId, titleNote = titleNote, note = noteContent)
        database.update(note)
    }

    fun deleteNote(noteId: Int){
        database.deleteNote(noteId)
    }
}