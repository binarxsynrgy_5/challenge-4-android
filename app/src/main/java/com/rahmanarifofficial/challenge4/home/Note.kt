package com.rahmanarifofficial.challenge4.home

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "note_table")
data class Note(
    @PrimaryKey(autoGenerate = true)
    var noteId: Int? = null,
    @ColumnInfo(name = "title_note")
    var titleNote: String,
    @ColumnInfo(name = "note")
    var note: String? = null
)
