package com.rahmanarifofficial.challenge4.home

import android.app.Dialog
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import com.rahmanarifofficial.challenge4.R
import com.rahmanarifofficial.challenge4.databinding.FragmentDeleteDialogBinding

class DeleteDialogFragment(private val action: () -> Unit) : DialogFragment() {

    private var _binding: FragmentDeleteDialogBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentDeleteDialogBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        dialog!!.window!!.setGravity(Gravity.CENTER_HORIZONTAL or Gravity.CENTER_VERTICAL)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)
        val dialog = Dialog(requireContext(), R.style.DialogFragmentStyleFade)
        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnCancelDeleteNote.setOnClickListener { dismiss() }
        binding.btnDeleteNote.setOnClickListener {
            action()
            dismiss()
        }
    }

    companion object {
        fun show(activity: FragmentActivity?, action: () -> Unit) {
            activity?.supportFragmentManager?.apply {
                val ft = beginTransaction()
                val fragment =
                    DeleteDialogFragment(action)
                fragment.show(ft, "dialog")
            }
        }
    }
}