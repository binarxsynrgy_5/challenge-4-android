package com.rahmanarifofficial.challenge4.home

import android.app.Dialog
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import com.rahmanarifofficial.challenge4.R
import com.rahmanarifofficial.challenge4.databinding.FragmentAddEditNoteDialogBinding


class AddEditNoteDialogFragment(
    var isAdd: Boolean,
    var title: String,
    var action: (Boolean, String, String) -> Unit,
    var titleNote: String? = null,
    var contentNote: String? = null
) : DialogFragment() {

    private var _binding: FragmentAddEditNoteDialogBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentAddEditNoteDialogBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        dialog!!.window!!.setGravity(Gravity.CENTER_HORIZONTAL or Gravity.CENTER_VERTICAL)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)
        val dialog = Dialog(requireContext(), R.style.DialogFragmentStyleFade)
        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isCancelable = true
        binding.titleDialog.text = title
        binding.editTextTitleNote.setText(titleNote)
        binding.editTextNote.setText(contentNote)
        binding.btnAddNote.text = title
        binding.btnAddNote.setOnClickListener {
            action(
                isAdd,
                binding.editTextTitleNote.text.toString(),
                binding.editTextNote.text.toString()
            )
            dismiss()
        }
    }

    companion object {
        fun show(
            activity: FragmentActivity?,
            isAdd: Boolean,
            titleDialog: String,
            titleNote: String? = null,
            contentNote: String? = null,
            action: (Boolean, String, String) -> Unit
        ) {
            activity?.supportFragmentManager?.apply {
                val ft = beginTransaction()
                val fragment =
                    AddEditNoteDialogFragment(isAdd, titleDialog, action, titleNote, contentNote)
                fragment.show(ft, "dialog")
            }
        }
    }
}