package com.rahmanarifofficial.challenge4.home

import android.app.Application
import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rahmanarifofficial.challenge4.database.AppDatabase
import com.rahmanarifofficial.challenge4.database.NoteDao

class HomeViewModelFactory(
    private val dataSource: NoteDao
) : ViewModelProvider.Factory {
    companion object {
        @Volatile
        private var instance: HomeViewModelFactory? = null

        fun getInstance(context: Context): HomeViewModelFactory =
            instance ?: synchronized(this) {
                instance ?: HomeViewModelFactory(
                    AppDatabase.getInstance(context).noteDao
                )
            }
    }


    @Suppress("unchecked_cast")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {
            return HomeViewModel(dataSource) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}