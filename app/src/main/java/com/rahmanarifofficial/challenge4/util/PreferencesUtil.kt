package com.rahmanarifofficial.challenge4.util

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit

class PreferencesUtil {
    companion object {
        private val sharePref = PreferencesUtil()
        private val sharedPreferencesName = "NotePreferences"
        private lateinit var preferences: SharedPreferences

        fun getInstance(context: Context): PreferencesUtil {
            if (!::preferences.isInitialized) {
                synchronized(PreferencesUtil::class.java) {
                    if (!::preferences.isInitialized) {
                        preferences = context.getSharedPreferences(sharedPreferencesName, Activity.MODE_PRIVATE)
                    }
                }
            }
            return sharePref
        }
    }

    var isLogin: Boolean
        set(value) {
            preferences.edit {
                putBoolean("IS_LOGIN", value)
            }
        }
        get() = preferences.getBoolean("IS_LOGIN", false)

}