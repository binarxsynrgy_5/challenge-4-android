package com.rahmanarifofficial.challenge4.auth

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rahmanarifofficial.challenge4.database.AppDatabase
import com.rahmanarifofficial.challenge4.database.UserDao

class AuthViewModelFactory(
    private val dataSource: UserDao
) : ViewModelProvider.Factory {
    companion object {
        @Volatile
        private var instance: AuthViewModelFactory? = null

        fun getInstance(context: Context): AuthViewModelFactory =
            instance ?: synchronized(this) {
                instance ?: AuthViewModelFactory(
                    AppDatabase.getInstance(context).userDao
                )
            }
    }


    @Suppress("unchecked_cast")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AuthViewModel::class.java)) {
            return AuthViewModel(dataSource) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}