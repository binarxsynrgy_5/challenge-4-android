package com.rahmanarifofficial.challenge4.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.rahmanarifofficial.challenge4.R
import com.rahmanarifofficial.challenge4.databinding.FragmentRegisterBinding

class RegisterFragment : Fragment() {

    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    private lateinit var viewModel: AuthViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentRegisterBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObject()
        setupUI()
        setupEventUI()
    }

    private fun setupObject() {
        val factory = AuthViewModelFactory.getInstance(requireContext())
        viewModel = ViewModelProvider(requireActivity(), factory)[AuthViewModel::class.java]
    }

    private fun setupUI() {

    }

    private fun setupEventUI() {
        binding.btnRegister.setOnClickListener {
            viewModel.register(
                binding.editTextUsername.text.toString(),
                binding.editTextEmail.text.toString(),
                binding.editTextPassword.text.toString()
            )
            findNavController().popBackStack()
            findNavController().navigate(R.id.loginFragment)
        }
    }
}