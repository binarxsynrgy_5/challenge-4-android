package com.rahmanarifofficial.challenge4.auth

import androidx.lifecycle.ViewModel
import com.rahmanarifofficial.challenge4.database.UserDao

class AuthViewModel(
    val database: UserDao
): ViewModel() {
    fun register(username: String, email: String, password: String) {
        val user = User(username = username, email = email, password = password)
        database.insert(user)
    }

    fun login(username: String, password: String): User? {
        val user = database.get(username, password)
        return user
    }
}