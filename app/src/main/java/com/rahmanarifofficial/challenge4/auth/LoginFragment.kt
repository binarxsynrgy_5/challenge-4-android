package com.rahmanarifofficial.challenge4.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.rahmanarifofficial.challenge4.R
import com.rahmanarifofficial.challenge4.databinding.FragmentLoginBinding
import com.rahmanarifofficial.challenge4.util.PreferencesUtil

class LoginFragment : Fragment() {

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private lateinit var viewModel: AuthViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentLoginBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObject()
        setupEventUI()
    }

    private fun setupObject() {
        val factory = AuthViewModelFactory.getInstance(requireContext())
        viewModel = ViewModelProvider(requireActivity(), factory)[AuthViewModel::class.java]
    }

    private fun setupEventUI() {
        if (PreferencesUtil.getInstance(requireActivity().applicationContext).isLogin){
            findNavController().navigate(R.id.homeFragment)
        }
        binding.btnLogin.setOnClickListener {
            val user = viewModel.login(
                binding.editTextEmail.text.toString(),
                binding.editTextPassword.text.toString()
            )
            if (user != null) {
                PreferencesUtil.getInstance(requireActivity().applicationContext).isLogin = true
                findNavController().navigate(R.id.homeFragment)
            }
        }
        binding.textGoToRegister.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
    }
}