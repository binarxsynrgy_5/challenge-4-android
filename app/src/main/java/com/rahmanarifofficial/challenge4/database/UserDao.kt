package com.rahmanarifofficial.challenge4.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.rahmanarifofficial.challenge4.auth.User

@Dao
interface UserDao {
    @Insert
    fun insert(note: User)

    @Query("SELECT * from user WHERE username = :username AND password = :password")
    fun get(username: String, password: String): User?
}