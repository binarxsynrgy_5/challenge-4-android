package com.rahmanarifofficial.challenge4.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.rahmanarifofficial.challenge4.home.Note

@Dao
interface NoteDao {
    @Insert
    fun insert(note: Note)

    @Update
    fun update(note: Note)

    @Query("DELETE FROM note_table where noteId = :noteId")
    fun deleteNote(noteId: Int)

    @Query("SELECT * FROM note_table ORDER BY noteId DESC")
    fun getAllNote(): LiveData<List<Note>>

}